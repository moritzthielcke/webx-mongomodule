/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package io.wx.modules.mongo.handler; 

import org.vertx.java.core.Handler;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.json.JsonObject;

/**
 *
 * @author moritz
 */
public abstract class MongoResponseHandler implements Handler<Message<JsonObject>>{ 
    protected JsonObject response;
    protected Boolean success=false;
    protected Exception exception;
   // protected String errorMsg;
    
    //protected String bodyAsString;

    public JsonObject getResponse() {
        return response;
    }

    public Boolean isSuccess() {
        return success;
    }

    /**
     * 
     * @return an exception raised in the handle() method
     */
    public Exception getException() {
        return exception;
    }

    /**
     * 
     * @return the error message , if mongodb returns one 
    
    public String getErrorMsg() {
        return errorMsg;
    } */

    
    
    
    public abstract void finish();

    
    
}
