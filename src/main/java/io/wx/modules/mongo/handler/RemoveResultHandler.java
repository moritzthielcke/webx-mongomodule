/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package io.wx.modules.mongo.handler;

import io.wx.modules.mongo.Status;
import io.wx.modules.mongo.exception.DataBaseException;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.json.JsonObject;

/**
 *
 * @author moritz
 */
public abstract class RemoveResultHandler extends MongoResponseHandler{
    private Integer resultCount = 0;
    
    public RemoveResultHandler(){
        
    }    
    
    
    @Override
    public void handle(Message<JsonObject> e) {
       try{
            if (Status.OK.getValue().equals(e.body().getString("status"))) {
                resultCount = e.body().getInteger("number");
                success = true;
            }
            else{
                throw new DataBaseException(e.body().encodePrettily());
            }
       }catch(Exception ex){
           this.exception = ex;
       }
       finish();
    }
    

    public Integer getResultCount() {
        return resultCount;
    }
   
}
