/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package io.wx.modules.mongo.handler;

import io.wx.core.HTTP.impl.WXTools;
import io.wx.modules.mongo.Status;
import io.wx.modules.mongo.exception.DataBaseException;
import java.util.ArrayList;
import java.util.List;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.json.JsonArray;
import org.vertx.java.core.json.JsonObject;

/**
 *
 * @author moritz
 */
public abstract class ArrayResultHandler<T> extends MongoResponseHandler{
    private final List<T> results;
    private final Class<?> type;  
    
    public ArrayResultHandler(Class<?> type){
        this.results = new ArrayList<T>();
        this.type = type;
    }
    
    @Override
    public void handle(Message<JsonObject> e) {
        try{   
            this.response = e.body();
            if (Status.OK.getValue().equals(e.body().getString("status"))) {
                JsonArray jsonResult =  e.body().getArray("results");
                if (jsonResult != null) {
                    for(int i=0; i <jsonResult.size(); i++){
                        JsonObject result = jsonResult.get(i);          
                        results.add( (T) WXTools.mapper.readValue(result.encode(), type) );
                    }
                }
                success = true;   
            } 
            else{
                throw new DataBaseException(e.body().encodePrettily());
            }
        }catch(Exception ex){
            this.exception = ex;
        }
        finish();
    }
   

    public List<T> getResults() {
        return results;
    }


    
    
    
    
}
