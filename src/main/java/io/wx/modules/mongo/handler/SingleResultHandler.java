/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.modules.mongo.handler;

import io.wx.core.HTTP.impl.WXTools;
import io.wx.modules.mongo.BaseEntity;
import io.wx.modules.mongo.Status;
import io.wx.modules.mongo.exception.DataBaseException;
import io.wx.modules.mongo.exception.EntityNotFoundException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.json.JsonObject;

/**
 *
 * @author moritz
 * @param &lt;T&gt; extends {@link BaseEntity}
 */
public abstract class SingleResultHandler<T extends BaseEntity> extends MongoResponseHandler {

    private T entity;
    private Class<?> entityClass;

    public SingleResultHandler() {
        this(null);
    }

    /***
     * 
     * @param entityClass the entity class, if null we try to get the class from ParameterizedType T
     */
    @SuppressWarnings("UnusedAssignment")    
    public SingleResultHandler(Class<? extends BaseEntity> entityClass) {
        if (entityClass == null) {
            Type gs = getClass().getGenericSuperclass();
            if (gs != null && gs instanceof ParameterizedType) {
                ParameterizedType pt = (ParameterizedType) gs;
                // there is and must be only one Type
                Type t = pt.getActualTypeArguments()[0];
                this.entityClass = (Class<T>) t;
            } else {
                throw new TypeNotPresentException("<T extends BaseEntity> not met", exception);
            }
        }
        else {
            this.entityClass = entityClass;
        }
    }
    
    

    @Override
    public void handle(Message<JsonObject> e) {
        try {
            this.response = e.body();
            if (Status.OK.getValue().equals(e.body().getString("status"))) {
                JsonObject result = e.body().getObject("result");
                if (result != null) {
                    this.entity = (T) WXTools.mapper.readValue(result.encode(), entityClass);
                } else {
                    throw new EntityNotFoundException();
                }
                success = true;
            } else {
                throw new DataBaseException(e.body().encodePrettily());
            }
        } catch (Exception ex) {
            this.exception = ex;
        }
        finish();
    }

    public T getEntity() {
        return entity;
    }
}
