/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package io.wx.modules.mongo.handler;

import io.wx.modules.mongo.Status;
import io.wx.modules.mongo.exception.DataBaseException;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.json.JsonArray;
import org.vertx.java.core.json.JsonObject;

/**
 *
 * @author moritz
 */
public abstract class RawArrayResultHandler extends MongoResponseHandler{
    private  JsonArray jsonResult;
    
    @Override
    public void handle(Message<JsonObject> e) {
        try{   
            this.response = e.body();
            if (Status.OK.getValue().equals(e.body().getString("status"))) {
                this.jsonResult =  e.body().getArray("results");
                success = true;   
            } 
            else{
                throw new DataBaseException(e.body().encodePrettily());
            }
        }catch(Exception ex){
            this.exception = ex;
        }
        finish();
    }

    public JsonArray getJsonResult() {
        return jsonResult;
    }
    
    
    
}
