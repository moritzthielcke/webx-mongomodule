/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package io.wx.modules.mongo.handler;

import io.wx.modules.mongo.BaseEntity;
import io.wx.modules.mongo.exception.NoIDReturnedException;
import io.wx.modules.mongo.Status;
import io.wx.modules.mongo.exception.SaveException;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.json.JsonObject;

/**
 *
 * @author moritz
 */
public abstract class SaveResultHandler<T extends BaseEntity> extends MongoResponseHandler{
    protected final T entity;
    
    public SaveResultHandler(T entity){
        this.entity = entity;
    }    
    
    
    @Override
    public void handle(Message<JsonObject> e) {
       try{
            if (Status.OK.getValue().equals(e.body().getString("status"))) {
                if(e.body().getString("_id") != null){
                    this.entity.setId( e.body().getString("_id") );
                }
                if(this.entity.getId()==null){
                    throw new NoIDReturnedException("No ID returned in Body => "+e.body().encodePrettily());
                }
                success=true;
            }
            else{
                JsonObject message = new JsonObject( e.body().getString("message") ); 
                if (message != null) {
                    this.exception = new SaveException(message.getString("err") + ". ErrorCode=" + message.getInteger("code"));
                }
                else {
                    this.exception = new SaveException("Error saving the entity");
                }
            }
       }catch(Exception ex){
           if(ex instanceof NoIDReturnedException){
               this.exception = ex;
           }
           this.exception = new SaveException(e.body().encode(),ex); 
       }
       finish();
    }

    
    public T getEntity() {
        return entity;
    }
       
    
    
}
