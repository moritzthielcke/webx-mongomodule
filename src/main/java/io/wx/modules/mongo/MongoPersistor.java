/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package io.wx.modules.mongo;

import io.wx.modules.mongo.exception.MissingEntityAnnotationException;
import io.wx.core.HTTP.impl.WXTools;
import java.io.IOException;
import java.util.Set;
import org.apache.log4j.Logger;
import org.reflections.Reflections;
import org.vertx.java.core.Handler;
import org.vertx.java.core.eventbus.EventBus;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.json.JsonArray;
import org.vertx.java.core.json.JsonObject;

/**
 *
 * @author moritz
 */
public class MongoPersistor {
    private static Logger logger = Logger.getLogger(MongoPersistor.class);
    private final EventBus eb;
    private final String mongodb;    

    
    
    public MongoPersistor(EventBus eb, String mongodb) {
        this.eb = eb;
        this.mongodb = mongodb;
    }
    

    
    public void getById(Class type, String id, Handler<Message<JsonObject>> callBack) throws MissingEntityAnnotationException {
        JsonObject matcher = new JsonObject();
        matcher.putString("_id", id);
        JsonObject cmd = new JsonObject();
        cmd.putString("action", "findone");
        cmd.putString("collection", getCollectionName(type));
        cmd.putObject("matcher", matcher);    
        send (cmd, callBack);
    }
 
    
    public void getByIdAndTenant(Class type, String id, String tenant, Handler<Message<JsonObject>> callBack) throws MissingEntityAnnotationException {
        JsonObject matcher = new JsonObject();
        matcher.putString("_id", id);
        matcher.putString("tenant", tenant);
        JsonObject cmd = new JsonObject();
        cmd.putString("action", "findone");
        cmd.putString("collection", getCollectionName(type));
        cmd.putObject("matcher", matcher);    
        send (cmd, callBack);
    }
    
    
    public void remove(Class type, String id, Handler<Message<JsonObject>> callBack) throws MissingEntityAnnotationException {
        JsonObject matcher = new JsonObject();
        matcher.putString("_id", id);
        remove(type, matcher, callBack);
    }
    
    public void remove(Class type, JsonObject matcher, Handler<Message<JsonObject>> callBack) throws MissingEntityAnnotationException {
        JsonObject cmd = new JsonObject();
        cmd.putString("action", "delete");
        cmd.putString("collection", getCollectionName(type));
        cmd.putObject("matcher", matcher);
        send (cmd, callBack);
    }
    
    public void find(Class type, JsonObject matcher, Handler<Message<JsonObject>> callBack) throws MissingEntityAnnotationException{
        JsonObject cmd = new JsonObject();
        cmd.putString("action", "find");
        cmd.putString("collection", getCollectionName(type));
        cmd.putObject("matcher", matcher);
        send (cmd, callBack);
    }
    
    public void findOne(Class type, JsonObject matcher, Handler<Message<JsonObject>> callBack) throws MissingEntityAnnotationException{
        JsonObject cmd = new JsonObject();
        cmd.putString("action", "findone");
        cmd.putString("collection", getCollectionName(type));
        cmd.putObject("matcher", matcher);
        send (cmd, callBack);
    }   
    
    public void findAndModify(Class type, JsonObject matcher, JsonObject update, Handler<Message<JsonObject>> callBack) throws MissingEntityAnnotationException{
        JsonObject cmd = new JsonObject();
        cmd.putString("action", "find_and_modify");
        cmd.putString("collection", getCollectionName(type));
        cmd.putObject("matcher", matcher);
        cmd.putObject("update", update);
        cmd.putBoolean("new", true); // to return the document with the modifications made on the update
        send (cmd, callBack);
    }   
    
    public void create(Object o, Handler<Message<JsonObject>> callBack) throws MissingEntityAnnotationException, IOException {
        JsonObject cmd = new JsonObject();
        cmd.putString("action", "save");
        cmd.putString("collection",  getCollectionName(o));
        cmd.putObject("document" , new JsonObject(WXTools.toJSON(o)) ); 
        send(cmd, callBack);
    }
    
    
    public void createIndex(JsonObject index , Class type) throws MissingEntityAnnotationException{
        JsonArray userIndexes = new JsonArray();
        userIndexes.add(index);
        createIndexes(userIndexes, type);
    }
    
    public void createIndexes(JsonArray indexes, Class type) throws MissingEntityAnnotationException{
        JsonObject createIndexCMD = new JsonObject();
        final String collectionName = getCollectionName(type);
        createIndexCMD.putString("createIndexes", collectionName);                       
        createIndexCMD.putArray("indexes", indexes);
        JsonObject action = new JsonObject();
        action.putString("action", "command");
        action.putString("command", createIndexCMD.encode());
        //logger.info("> enshuring indexes on "+collectionName);
        send(action, new Handler<Message<JsonObject>>() {
            @Override
            public void handle(Message<JsonObject> e) {        
               String msg = " > MongoDB create Index on "+collectionName;
               if("ok".equals(e.body().getString("status"))){
                    logger.info(msg+" [ok] "+e.body().getObject("result").encode());
               }
               else{
                   logger.info(msg+" [fail] "+e.body().encode());
               }
            }
        });        
    }

    
    public void send(JsonObject cmd, Handler<Message<JsonObject>> callback){
        eb.send(mongodb, cmd, callback);
    }
    

    public void createIndexes(Reflections reflections){
        createIndexes( reflections.getTypesAnnotatedWith(Entity.class) );
    }
       
    
    public void createIndexes( Set<Class<?>> entitys){
        for(Class e : entitys){      
            try {
                //@todo how to make that more sexy?
                Object o = e.newInstance();
                JsonArray indexes = ((BaseEntity)o).describeIndexes();  
                if(indexes != null){
                    createIndexes(indexes, e);
                }                   
            }
            catch(ClassCastException cex){
                       logger.warn(e.getName()+" does not use BaseEntity.class, skipping index creation");
            } 
            catch(Exception ex){
                logger.error("cant create index on "+e.getName(),ex);
            }    
        }
    }
 
    
    public static String getCollectionName(Object o) throws MissingEntityAnnotationException{
        Entity entityAnnotation;
        if( ( entityAnnotation = (Entity) o.getClass().getAnnotation(Entity.class) ) == null ){
            throw new MissingEntityAnnotationException("missing Entity Annotation on class "+o.getClass());
        }
        return entityAnnotation.collection();
    }
    
    
    public static String getCollectionName(Class type) throws MissingEntityAnnotationException{
        Entity entityAnnotation;
        if( ( entityAnnotation = (Entity)type.getAnnotation(Entity.class) ) == null ){
            throw new MissingEntityAnnotationException("missing Entity Annotation on class "+type);
        }
        return entityAnnotation.collection();
    }
    
    
}
