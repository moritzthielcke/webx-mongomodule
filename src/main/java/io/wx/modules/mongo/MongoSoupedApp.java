/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package io.wx.modules.mongo;

import io.wx.core.HTTP.impl.Core;
import io.wx.core.cloud.CloudApp;
import io.wx.core.cloud.CloudConfig;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.vertx.java.core.AsyncResult;
import org.vertx.java.core.AsyncResultHandler;
import org.vertx.java.core.json.JsonObject;

/**
 *
 * @author moritz
 */
public abstract class MongoSoupedApp extends CloudApp{
    public MongoPersistor mongoPersistor;
    public String bussAddr;
    private String moduleName;
    public Core core;

    public MongoSoupedApp(String bussAddr, String moduleName) throws CloudConfig.ConfigurationException, Exception {
        super();
        this.bussAddr = bussAddr;
        this.moduleName = moduleName;
        this.core = getCore();
    }
    
    public MongoPersistor getMongoPersistor(){
        if(mongoPersistor==null){
            mongoPersistor = new MongoPersistor(this.core.getVertx().eventBus(), bussAddr);
        }
        return mongoPersistor;
    }    
    
    @Override
    public void start() throws Exception{
        super.start();
        core.getPlatformManager()
        .deployModule(moduleName, createPersistorCfg(), 1, 
            new AsyncResultHandler<String>() {
                public void handle(AsyncResult<String> asyncResult) {
                    if (asyncResult.succeeded()) {                
                        logger.info(" > mongo-persistor deployed" +asyncResult.result());
                        logger.info(" > creating indexes");
                        /* create my app-login index (email + application) */
                        getMongoPersistor().createIndexes(core.reflections);            
                    } else {
                       logger.error( asyncResult.cause() );
                    }
                }
            }
        );
    }

    private JsonObject createPersistorCfg(){
        JsonObject persistorCfg = new JsonObject();
        Map<String, String> credentials = this.getConfig().getCrendentials().getAddonCredentials("mongosoup");
        //Map<String, String> credentials = new HashMap<String, String>();
        //credentials.put("MONGOSOUP_URL", "mongodb://APP_NAME:PASSWORD@dbs001.mongosoup.de:99//DBNAME");
        if (credentials != null && credentials.get("MONGOSOUP_URL") != null) {
            Matcher matcher = Pattern.compile( "mongodb://([^:]*):([^@]*).([^/]*)//(.*)" ).matcher( credentials.get("MONGOSOUP_URL") );
            if(matcher.find() && matcher.groupCount()==4){//
                persistorCfg.putString("username", matcher.group(1));
                persistorCfg.putString("password", matcher.group(2));
                String[] getHost = matcher.group(3).split(":");
                persistorCfg.putString("host", getHost[0]);
                if(getHost.length>1){
                    persistorCfg.putNumber("port", new Integer(getHost[1]));    
                }
                persistorCfg.putString("db_name", matcher.group(4));
            }
            else{
                logger.fatal("invalid MONGOSOUP_URL format");
            }
            persistorCfg.putString("address", bussAddr);         
        }        
        else{
            logger.fatal("mongosoup configuration missing");
        }
        return persistorCfg;
    }
    
    
}
