/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package io.wx.modules.mongo.exception;

/**
 *
 * @author moritz
 */
public class SaveException extends Exception {
    
    
    /**
     * Creates a new instance of <code>SaveException</code> without detail
     * message.
     */
    public SaveException() {
    }

    /**
     * Constructs an instance of <code>SaveException</code> with the specified
     * detail message.
     *
     * @param msg the detail message.
     */
    public SaveException(String msg) {
        super(msg);
    }
    
    public SaveException(String msg, Throwable cause) {
        super(msg,cause);
    }

    
    
}
