/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package io.wx.modules.mongo.exception;

/**
 *
 * @author moritz
 */
public class NoIDReturnedException extends Exception {

    /**
     * Creates a new instance of <code>NoIDReturnedException</code> without
     * detail message.
     */
    public NoIDReturnedException() {
    }

    /**
     * Constructs an instance of <code>NoIDReturnedException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public NoIDReturnedException(String msg) {
        super(msg);
    }
}
