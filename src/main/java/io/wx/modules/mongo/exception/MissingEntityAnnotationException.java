/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package io.wx.modules.mongo.exception;

/**
 *
 * @author moritz
 */
public class MissingEntityAnnotationException extends Exception {

    /**
     * Creates a new instance of <code>MissingEntityAnnotationException</code>
     * without detail message.
     */
    public MissingEntityAnnotationException() {
    }

    /**
     * Constructs an instance of <code>MissingEntityAnnotationException</code>
     * with the specified detail message.
     *
     * @param msg the detail message.
     */
    public MissingEntityAnnotationException(String msg) {
        super(msg);
    }
}
