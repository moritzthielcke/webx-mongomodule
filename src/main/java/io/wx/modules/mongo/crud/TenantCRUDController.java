package io.wx.modules.mongo.crud;

import io.wx.core.HTTP.Controller;
import io.wx.core.HTTP.DELETE;
import io.wx.core.HTTP.GET;
import io.wx.core.HTTP.HandleResponseException;
import io.wx.core.HTTP.POST;
import io.wx.core.HTTP.PUT;
import io.wx.core.HTTP.impl.WXTools;
import io.wx.modules.mongo.BaseEntity;
import io.wx.modules.mongo.MongoPersistor;
import io.wx.modules.mongo.exception.EntityNotFoundException;
import io.wx.modules.mongo.exception.NoIDReturnedException;
import io.wx.modules.mongo.handler.ArrayResultHandler;
import io.wx.modules.mongo.handler.RemoveResultHandler;
import io.wx.modules.mongo.handler.SaveResultHandler;
import io.wx.modules.mongo.handler.SingleResultHandler;
import org.apache.log4j.Logger;
import org.vertx.java.core.json.JsonObject;

/**
 *
 * @author Jose Luis Conde <jose.linares@code-mitte.de>
 * @param <T>
 */
public abstract class TenantCRUDController<T extends BaseEntity> extends Controller {

    private static final Logger logger = Logger.getLogger(TenantCRUDController.class);
    

    private T baseEntity;

    public T getBaseEntity() {
        return baseEntity;
    }

    public void setBaseEntity(T baseEntity) {
        this.baseEntity = baseEntity;
    }

    public abstract MongoPersistor getMongoPersistor();

    public abstract String getTenantID();

    @GET(path = "/:id")
    public void get() {
        final String id = this.requestController.getRequest().params().get("id");
        try {
            MongoPersistor mongoPersistor = getMongoPersistor();
            mongoPersistor.getByIdAndTenant(getBaseEntity().getClass(), id, getTenantID(),
                    new SingleResultHandler<T>(getBaseEntity().getClass()) {
                        @Override
                        public void finish() {
                            if (getEntity() != null) {
                                handleResponse(getEntity());
                            } else {
                                handleError("Error querying the entity with id " + id,getException());
                            }
                        }
                    });
        } catch (Exception ex) {
            WXTools.sendServerErrorAsJSON(requestController, null, requestController.DEBUG, ex);
        }
    }

    @GET
    public void listAll() {
        try {
            JsonObject query = getRequestParams();
            MongoPersistor mongoPersistor = getMongoPersistor();
            mongoPersistor.find(getBaseEntity().getClass(), query,
                    new ArrayResultHandler<T>(getBaseEntity().getClass()) {
                        @Override
                        public void finish() {
                            if (success) {
                                handleResponse(getResults());
                            } else {
                                handleError("cant list "+getBaseEntity().getClass(), getException());
                            }
                        }
                    });

        } catch (Exception ex) {
            logger.error(ex);
            WXTools.sendServerErrorAsJSON(requestController, null, requestController.DEBUG, ex);
        }
    }

    @DELETE(path = "/:id")
    public void delete() {
        final String id = this.requestController.getRequest().params().get("id");
        try {
            MongoPersistor mongoPersistor = getMongoPersistor();

            mongoPersistor.getByIdAndTenant(getBaseEntity().getClass(), id, getTenantID(),
                    new SingleResultHandler<T>(getBaseEntity().getClass()) {
                        @Override
                        public void finish() {
                            if (success && entityFound(getEntity())) {
                                removeEntity(getEntity().getId());
                            } else {
                                handleError("cant delete entity with id " + id, getException());
                            }
                        }
                    });
        } catch (Exception ex) {
            logger.error(ex);
            WXTools.sendServerErrorAsJSON(requestController, null, requestController.DEBUG, ex);
        }
    }

    private void removeEntity(final String idEntity) {
        try {
            MongoPersistor mongoPersistor = getMongoPersistor();
            mongoPersistor.remove(getBaseEntity().getClass(), idEntity,
                new RemoveResultHandler() {
                    @Override
                    public void finish() {
                        if (success) {
                            handleResponse(getResultCount().toString());
                        } else {
                            handleError("cant delete entity with id "+idEntity, getException());
                        }
                    }
                });
        } catch (Exception ex) {
            logger.error(ex);
            WXTools.sendServerErrorAsJSON(requestController, null, requestController.DEBUG, ex);
        }
    }

    public void saveEntity(T entityToCreate) {
        try {
            if (entityToCreate != null) {
                //The tenant can not be modified
                entityToCreate.setTenant(getTenantID());
                MongoPersistor mongoPersistor = getMongoPersistor();
                mongoPersistor.create(entityToCreate, new SaveResultHandler<T>(entityToCreate) {
                    @Override
                    public void finish() {
                        if (success) {
                            handleResponse(getEntity());
                        } else {
                            handleError("cant save entity", getException());
                        }
                    }
                });
            } else {
                WXTools.sendServerErrorAsJSON(requestController, "entity is null", requestController.DEBUG, null);
            }
        } catch (Exception ex) {
            logger.error(ex);
            WXTools.sendServerErrorAsJSON(requestController, null, requestController.DEBUG, ex);
        }
    }

        
    @POST
    public void create() {
        T entityToCreate = null;
        try {
                entityToCreate = (T) WXTools.mapper.readValue(this.requestController.getRequestBody(), getBaseEntity().getClass());
        } catch (Exception ex) {
            logger.error(ex);
            WXTools.sendServerErrorAsJSON(requestController, null, requestController.DEBUG, ex, 400);
        }
        saveEntity(entityToCreate);
    }

    
    @PUT
    public void update() {
        T entityToCreate = null;
        try {
            entityToCreate = (T) WXTools.mapper.readValue(this.requestController.getRequestBody(), getBaseEntity().getClass());
        } catch (Exception ex) {
            logger.error(ex);
            WXTools.sendServerErrorAsJSON(requestController, null, requestController.DEBUG, ex, 400);
        }
        saveEntity(entityToCreate);
    }

    
    public boolean entityFound(BaseEntity entity) {
        return entity != null && entity.getId() != null;
    }

     
    public JsonObject getRequestParams() {
        JsonObject params = new JsonObject();
        for (String param : this.requestController.getRequest().params().names()) {
            params.putString(param, this.requestController.getRequest().params().get(param));
        }
        //always filter on a fixed tenant
        params.putString("tenant", getTenantID());
        return params;
    }   
    
    public void handleResponse(Object objectToResponse) {
        try {
            this.requestController.doResponse(objectToResponse);
            requestController.doAfter();
        } catch (HandleResponseException ex) {
            logger.error(ex);
            WXTools.sendServerErrorAsJSON(requestController, null, requestController.DEBUG, ex);
        }
    }

    public void handleError(String error, Exception ex) {
       logger.error(error,ex);
       
       int statusCode = 500;
        if (ex instanceof EntityNotFoundException) {
            statusCode = 404;
        }
        else if (ex instanceof NoIDReturnedException) {
            statusCode = 500;
        }
        
        WXTools.sendServerErrorAsJSON(this.requestController, error, requestController.DEBUG, ex, statusCode);
    }

    
   /* protected List<String> getLanguagesFromRequestHeader() {
        List<String> languages = new ArrayList<>();
        HttpServerRequest request = this.requestController.getRequest();
        if (request.headers().get("Accept-Language") != null) {
            String languagesFromRequest = request.headers().get("Accept-Language");
            languagesFromRequest = languagesFromRequest.replaceAll(";q=.{3}", "");
            languages = Arrays.asList(languagesFromRequest.split(","));

        }
        return languages;
    }*/

}
