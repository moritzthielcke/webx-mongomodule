/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package io.wx.modules.mongo;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import org.vertx.java.core.json.JsonArray;

/**
 *
 * @author moritz
 */
public class BaseEntity implements Serializable{
    
    @JsonProperty("_id")
    private String id;
    
    private String tenant;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public String getTenant() {
        return tenant;
    }

    public void setTenant(String tenant) {
        this.tenant = tenant;
    }
    
    public JsonArray describeIndexes(){
        return null;
    }
    
}
