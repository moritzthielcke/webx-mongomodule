/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package io.wx.modules.mongo;

/**
 *
 * @author moritz
 */
public enum Status {
    OK("ok"),
    ERROR("error");

    private String value;

    private Status(String value){
        this.value = value;
    }

    public String getValue() {
        return value;
    }


    @Override
    public String toString() {
        return value;
    }

}
