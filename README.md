webx mongo persitor module

authors:
@dittmarsteiner 
@jose_linares 
@moritzthielcke

needs:

        <dependency>         
            <groupId>io.vertx</groupId>
            <artifactId>mod-mongo-persistor</artifactId>
            <version>2.1.1</version>
            <scope>compile</scope> 
        </dependency>